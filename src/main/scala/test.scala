package test

import javax.ws.rs.{GET, Produces, Path}

import scala.util.Random

@Path("/helloworld")
class TestResource {
  @GET
  def hello() = {
    "HELLO!!"
  }
}

@Path("/password")
@Produces(Array("application/json"))
class PasswordResource {
  def alphabet = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') ++ "~!@#$%^&*()-_=+`|".toCharArray
  def r = new Random
  @GET
  def generatePassword() = {
    (1 to 8).map((_)=>alphabet(r.nextInt(alphabet.length))).mkString
  }
}
