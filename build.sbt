name := "test"

libraryDependencies += "javax.servlet" % "servlet-api" % "2.5" % "provided"

libraryDependencies += "org.mortbay.jetty" % "jetty" % "6.1.22" % "container"

libraryDependencies += "com.sun.jersey" % "jersey-server" % "1.2"

libraryDependencies += "com.sun.jersey" % "jersey-json" % "1.2"

seq(webSettings :_*)



		    